# React Responsive Context
This is a simple React library for reacting to responsive events. It's super simple and only uses ~1kB gzipped
Works for React Dom or React Native (see below for RN usage)

### Basic Usage
Include the provider for giving the context
```jsx
import {ResponsiveProvider} from "react-responsive-context";

ReactDOM.render(<ResponsiveProvider><App/></ResponsiveProvider>, document.getElementById('root'));
```

Use it in your components
```jsx
import {ResponsiveConsumer} from "react-responsive-context";

() =>
  <ResponsiveConsumer>{
    (status) =>
      <div>
        <Button
          size={status.mobile ? "small" : "large"}>
          Click me
        </Button>
        {status.desktop && <p>This only shows in Desktop</p>}
      </div>
  }</ResponsiveConsumer>
```
### React Native

For React native make sure to import `ResponsiveConsumer` and `ResponsiveProvider` from `react-responsive-context/es/native` instead of `react-responsive-context`.
```jsx
import {ResponsiveProvider, ResponsiveConsumer} from "react-responsive-context/es/native";
```

### Default sizes and configuration

Default sizes:
 - **mobile**: up to 767px
 - **tablet**: from 768px to 1023px
 - **desktop**: from 1024px

you can set any ranges you want like this
```jsx
import {ResponsiveProvider} from "react-responsive-context";

const sizes = {
  small: [-Infinity, 500],
  medium: [501, 1000],
  large: [1001, +Infinity]
};

ReactDOM.render(<ResponsiveProvider sizes={sizes}><App/></ResponsiveProvider>, document.getElementById('root'));
```

### Any help is welcomed !