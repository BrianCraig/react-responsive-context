import {ResponsiveContext, ResponsiveProviderBase} from "./responsive";
import {Dimensions} from "react-native";

export const ResponsiveConsumer = ResponsiveContext;

export class ResponsiveProvider extends ResponsiveProviderBase {
  width(){
    return Dimensions.get('window').width;
  }

  componentDidMount() {
    Dimensions.addEventListener("change", this.onWindowResize);
  }

  componentWillUnmount() {
    Dimensions.removeEventListener("change", this.onWindowResize);
  }
}

