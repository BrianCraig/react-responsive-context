import React from "react";

const oMap = (o, f) => Object.assign({}, ...Object.keys(o).map(k => ({ [k]: f(o[k]) })))

const defaultSizes = {
  mobile: [-Infinity, 767],
  tablet: [768, 1023],
  desktop: [1024, +Infinity]
};

export const ResponsiveContext = React.createContext({});

export class ResponsiveProviderBase extends React.PureComponent{
  constructor(props){
    super(props);
    this.state = this.actualResponsiveState()
  }

  actualResponsiveState(){
    const width = this.width();
    return oMap(this.props.sizes, v => (width >= v[0]) && (width <= v[1]));
  }

  render(){
    return <ResponsiveContext.Provider value={this.state}>{
      this.props.children
    }</ResponsiveContext.Provider>
  }

  onWindowResize = () => this.setState(this.actualResponsiveState());
}

ResponsiveProviderBase.defaultProps = {
  sizes: defaultSizes
};

